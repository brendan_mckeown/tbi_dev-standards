#Javascript Development Standards
This document is largely based on Douglas Crockford's [Code Conventions for JavaScript](http://javascript.crockford.com/code.html). Changes have been made where necessary to merge it with the standards typically used in jQuery code. When changes were not necessary, Douglas Crockford's own words were left untouched.

All of our JavaScript code is sent directly to the public. It should always be of publication quality. Neatness counts. 

##Table of Contents

  1. [General Authoring](general-authoring.md)
  1. [jQuery Best Practices](jquery-best-practices.md)
  1. [Structuring Large Client-side Applications](large-applications.md)
  1. MVC Frameworks (coming soon)
  1. [Resources](resources.md)